# cbrother-for-aliyun-oss-sdk

#### 介绍
cbrother-for-aliyun-oss-sdk 是基于aliyun-oss-php-sdk，使用CBrother语言重构的。
最近比较忙，只是对php代码进行简单重构，需要的童鞋可以自行修改代码


#### 安装教程
1.  在cbrother安装根目录lib文件夹下，克隆我使用CBrother封装的php函数项目，方便全局调用这些函数，参考：https://gitee.com/wenzhihzhon/cbrother-for-php-function
    比如，D:\cbrother_v2.4.5_win_amd64\lib\cbrotherForPhpFunction\php.cb
2.  克隆当前项目cbrother-for-aliyun-oss-sdk下来：
    ```git clone https://gitee.com/wenzhihzhon/cbrother-for-aliyun-oss-sdk.git```
    如果要把sdk引入自己项目，要修改项目名称，不能包含横杠，因为import到自己的业务代码会报错。

#### 使用说明
1.  添加oss 配置信息
    ```vim test/Config.cb```
2.  修改测试用例
    ```vim test/upload.cb```
    修改以下测试路径
    var $object = "wzz2019/example3.jpg";
    var $file = "D:\\Downloads\\example.jpg";
3.  测试命令
    ```D:\cbrother_v2.4.5_win_amd64\cbrother.exe  test/upload.cb```
    输出
    ```
    Map {
        "response_data" : "",
        "response_code" : 200,
        "request_url" : "http://wzz2019.oss-cn-beijing.aliyuncs.com/wzz2019/example3.jpg"
    }
    ```

#### 致谢
[CBrother官网](http://www.cbrother.net/main.html)
[阿里云OSS官方文档](https://help.aliyun.com/document_detail/31946.html)